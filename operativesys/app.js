const myapp = new Vue({
    el: "#app",
    data: () => ({
        code: "console.log('awesome code editor')"
    }),
    methods: {
        highlighter(code) {
            return Prism.highlight(code, Prism.languages.js, "js");
        }
    },
    async created() {
        const response = await fetch('./banker.js');
        const data = await response.text();
        this.code = `${data}`;
    },
    watch: {
        data: function(newSource, oldSource) {
            eval(newSource);
        }
    }
})