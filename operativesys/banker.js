// Vectores de recursos
const recursosExistentes = [4, 2, 3, 1];
const recursosDisponibles = [2, 1, 0, 0];

// Matrices de asignación y solicitud
const matrizSolicitud = [
  [2, 0, 0, 1],
  [1, 0, 1, 0],
  [2, 1, 0, 0]
];

const matrizAsignacionActual = [
  [0, 0, 1, 0],
  [2, 0, 0, 1],
  [0, 1, 2, 0]
];

// Paso 1: Inicializar el vector de trabajo
const trabajo = recursosDisponibles.slice();

// Paso 2: Crear un vector booleano para rastrear los procesos completados
const completados = matrizAsignacionActual.map(() => false);

// Paso 3: Iniciar el bucle principal
let contador = 0;
while (contador < matrizAsignacionActual.length) {
  let procesoEncontrado = false;

  // Paso 4: Buscar un proceso que pueda terminar
  for (let i = 0; i < matrizAsignacionActual.length; i++) {
    if (!completados[i] && matrizSolicitud[i].every((s, j) => s <= trabajo[j])) {
      procesoEncontrado = true;
      completados[i] = true;
      console.log('Asignacion actual',trabajo);
      console.table('Proceso', matrizSolicitud[i]);
      trabajo.forEach((t, j) => trabajo[j] = t + matrizAsignacionActual[i][j]);
      contador++;
      break;
    }
  }

  // Paso 5: Si no se encuentra un proceso que pueda terminar, el estado no es seguro
  if (!procesoEncontrado) {
    console.log("El estado no es seguro");
    break;
  }
}

// Si todos los procesos se completaron, el estado es seguro
if (contador === matrizAsignacionActual.length) {
  console.log("El estado es seguro");
}
