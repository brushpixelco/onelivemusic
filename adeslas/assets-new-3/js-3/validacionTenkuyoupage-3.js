if(localStorage.getItem(`modal-value`)){Swal.fire({icon:'info',text:'Ha ingresado este número anteriormente, para mayor información comunícate a la linea 018000415196',})
localStorage.removeItem('modal-value');}
if(localStorage.getItem(`modal-value-lead`)=='false'){Swal.fire({icon:'error',text:'Ha ocurrido un error, por favor inténtelo más tarde',})
localStorage.removeItem('modal-value-lead');}
if(localStorage.getItem(`modal-value-lead`)=='true'){Swal.fire({icon:'success',title:'¡Gracias por tu registro!',html:'<p style="font-size:1rem; font-weight:normal;">En un tiempo máximo de 30 minutos, uno de nuestros asesores se comunicará contigo. <br> <br> <strong>Horario de atención:</strong> Lunes a viernes de 08:00AM a 10:00PM - Sábados de 08:00AM a 08:00PM y domingos de 09:00AM a 06:00PM </p>',})
localStorage.removeItem('modal-value-lead');}