const consultaStrategia=()=>{const item=localStorage.getItem('strategy')
if(item==null){return ''}else{return item}}
const consultarGclid=()=>{const item=localStorage.getItem('mygclid')
if(item==null){return ''}else{return item}}
const consultarAnuncioGroup=()=>{const item=localStorage.getItem('anuncioGroup')
if(item==null){return ''}else{return item}}
const validarFormulario=(e,idName,idPhone,idEmail,idTerms,idTypeForm,segment)=>{e.preventDefault();var full_name_lead=document.getElementById(`${idName}`).value
var phone_lead=document.getElementById(`${idPhone}`).value
var terms_lead=document.getElementById(`${idTerms}`).checked
var email_lead=document.getElementById(`${idEmail}`).value
if(full_name_lead==''){Swal.fire({icon:'error',text:'Por favor ingresa tu nombre',})
return false}else if(full_name_lead.length<3){Swal.fire({icon:'error',text:'Por favor ingresa un nombre valido',})
return false}
let numero=validarNumeroTelefonico(phone_lead)
if(numero.ok==false){Swal.fire({icon:'error',text:numero.msg,})
document.getElementById(`${idbtn}`).disabled=false
return false}
if(phone_lead.charAt(0)!='3'){Swal.fire({icon:'error',text:'Por favor ingresa un numero valido, ingresa un numero inicioado en "3"',})
return false}
if(terms_lead==false){Swal.fire({icon:'error',text:'Por favor acepta los terminos y condiciones',})
return false}else{terms_lead='on'}
var dominio='internethogares.co/'
var estrategia=consultaStrategia()
var segment_lead=segment
document.getElementById('form-dominio-modal-planes').value=dominio
document.getElementById('form-estrategia-modal-planes').value=estrategia
document.getElementById('form-segment_lead-modal-planes').value=segment_lead
document.getElementById('form-type_segment-modal-planes').value=''
document.getElementById('form-gclid-planes').value=consultarGclid()
document.getElementById('form-anuncioGroup-planes').value=consultarAnuncioGroup()
document.getElementById('id-form-modal').submit()
return true}
const validarFormulariosPrincipal=(e,idbtn,idnombre,idnumero,idterminos,idstrategia,idsegmento,segmento='',idform,idgclid='',idgAnuncioGroup='')=>{e.preventDefault();document.getElementById(`${idbtn}`).disabled=true
var full_name_lead=document.getElementById(`${idnombre}`).value
var phone_lead=document.getElementById(`${idnumero}`).value
var terms_lead=document.getElementById(`${idterminos}`).checked
if(full_name_lead==''){Swal.fire({icon:'error',text:'Por favor ingresa tu nombre',})
document.getElementById(`${idbtn}`).disabled=false
return false}else if(full_name_lead.length<3){Swal.fire({icon:'error',text:'Por favor ingresa un nombre valido',})
document.getElementById(`${idbtn}`).disabled=false
return false}
let numero=validarNumeroTelefonico(phone_lead)
if(numero.ok==false){Swal.fire({icon:'error',text:numero.msg,})
document.getElementById(`${idbtn}`).disabled=false
return false}
if(terms_lead==false){Swal.fire({icon:'error',text:'Por favor acepta los terminos y condiciones',})
document.getElementById(`${idbtn}`).disabled=false
return false}else{terms_lead='on'}
if(localStorage.getItem('sementoParametro')=='social_media'){document.getElementById(`${idsegmento}`).value='social_media'}else{document.getElementById(`${idsegmento}`).value=segmento}
document.getElementById(`${idgclid}`).value=consultarGclid()
document.getElementById(`${idstrategia}`).value=consultaStrategia()
if(idgAnuncioGroup==''){alert('id anuncio group faltante por configurar')}else{document.getElementById(`${idgAnuncioGroup}`).value=consultarAnuncioGroup()}
document.getElementById(`${idform}`).submit()
return true}
const validarNumeroTelefonico=(numero)=>{if(numero==''){return{'ok':false,'msg':'Por favor ingresa tu número'}}else if(numero.length!=10){return{'ok':false,'msg':'Por favor ingresa tu número de 10 digitos'}}
if(numero.charAt(0)!='3'){return{'ok':false,'msg':'Por favor ingresa un número valido, ingresa un número inicioado en "3"'}}
if(numero)
return{'ok':true,'msg':'Número OK'}}
const soloNumeros=(string)=>{var out='';var filtro='1234567890';for(var i=0;i<string.length;i++)
if(filtro.indexOf(string.charAt(i))!=-1)
out+=string.charAt(i);return out;}